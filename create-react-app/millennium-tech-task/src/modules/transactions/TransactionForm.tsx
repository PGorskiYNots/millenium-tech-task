import React, { useState } from 'react';
import styles from './Transactions.module.css';
import { Box } from '@chakra-ui/react';
import { FormControl, FormLabel, Button, Input, Text } from '@chakra-ui/react';

import type { Transaction } from './hooks/useTransactions';

export const TransactionForm = ({
  submitTransaction,
}: {
  submitTransaction: (arg0: Transaction) => void;
}) => {
  const [form, setInput] = useState<Transaction>({
    amount: 0,
    account: 0,
    address: '',
    description: '',
    id: '',
    date: '',
  });

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setInput({
      ...form,
      [name]: value,
    });
  };

  const handleSubmit = () => {
    submitTransaction(form);
  };

  // could be stored in object, etc
  const amountValidationError = form.amount < 0;
  const accountValidationError = isNaN(form.account) || !form.account;
  const isError = amountValidationError || accountValidationError;

  return (
    <Box p={3} className={`${styles.transactionForm} ${styles.card}`}>
      <FormControl>
        <FormLabel>Amount</FormLabel>
        <Input
          type="text"
          name="amount"
          value={form.amount}
          onChange={handleInputChange}
        />
        {amountValidationError && (
          <Text fontSize="sm"> Amount must be positive. </Text>
        )}

        <FormLabel>Account number</FormLabel>
        <Input
          type="text"
          name="account"
          value={form.account}
          onChange={handleInputChange}
        />
        {accountValidationError && (
          <Text fontSize="sm">
            Account number should be numbers, not empty.
          </Text>
        )}

        <FormLabel>Address</FormLabel>
        <Input
          type="text"
          name="address"
          value={form.address}
          onChange={handleInputChange}
        />

        <FormLabel>Description</FormLabel>
        <Input
          type="text"
          name="description"
          value={form.description}
          onChange={handleInputChange}
        />

        <Button
          disabled={isError}
          mt={4}
          colorScheme="teal"
          type="submit"
          onClick={handleSubmit}
        >
          Submit
        </Button>
      </FormControl>
    </Box>
  );
};
