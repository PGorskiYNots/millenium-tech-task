import React, { useState } from 'react';
import styles from './Transactions.module.css';
import { Box } from '@chakra-ui/react';
import { FormControl, FormLabel, Input } from '@chakra-ui/react';

interface FilterProps {
  handleFiltering: (arg0: string) => void;
}

export const Filter = ({ handleFiltering }: FilterProps) => {
  const [input, setInput] = useState('');
  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInput(e.target.value);
    handleFiltering(e.target.value);
  };

  return (
    <Box p={4} className={`${styles.transactionForm} ${styles.card}`}>
      <FormControl>
        <FormLabel> Filter by Beneficiary</FormLabel>
        <Input
          type="text"
          name="filter"
          value={input}
          onChange={handleInputChange}
        />
      </FormControl>
    </Box>
  );
};
