import { useState, useEffect, useCallback } from 'react';
import { useToast } from '@chakra-ui/react';

// this should be in the service
const apiUrl = 'http://localhost:3004/transactions';

export type Transaction = {
  amount: number;
  account: number;
  address: string;
  description: string;
  date?: string;
  id?: string;
  beneficiary?: string;
};

export const useTransactions = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [transactions, setTransactions] = useState<Transaction[]>([]);
  const [inMemoTransactions, setInMemoTransactions] = useState<Transaction[]>(
    []
  ); // only for FE filtering needs
  // eslint-disable-next-line
  const [error, setError] = useState<unknown | null>(null);
  const toast = useToast();

  // handle filtering and pagination - normally this would be re-querying to backend
  const [filter, setFilter] = useState('');

  // pagination can be implemented or by passing hash from last element or by passing offset and size so BE can load more
  // eslint-disable-next-line
  const [pagination, setPagination] = useState(0);

  // initial fetch
  useEffect(() => {
    setIsLoading(true);
    const fetchData = async () => {
      try {
        const resp = await fetch(apiUrl);
        const respJson = await resp.json();
        setTransactions(respJson);
        setInMemoTransactions(respJson);
        setIsLoading(false);
      } catch (err: unknown) {
        setError(err);
        setIsLoading(false);
        toast({
          title: 'Error.',
          description: 'Failed to fetch transactions.',
          status: 'error',
          duration: 9000,
          isClosable: true,
        });
      }
    };

    fetchData();
  }, [toast]);

  // just for ticket purposes - normally should be handled as an async communication
  const submitTransaction = useCallback(
    ({ amount, account, address, description }: Transaction) => {
      const newTransaction = {
        amount,
        account,
        address,
        description,
        date: new Date().toJSON(),
        id: `${Math.floor(Math.random() * 1000)}`,
      };

      setTransactions([newTransaction, ...transactions]);
    },
    [transactions, setTransactions]
  );

  const removeTransaction = useCallback(
    (id: string) => {
      const filteredTransactions = transactions.filter(t => t.id !== id);
      setTransactions(filteredTransactions);
    },
    [transactions]
  );

  useEffect(() => {
    if (filter) {
      const queryTransactions = transactions.filter(t =>
        t.beneficiary?.includes(filter)
      );
      setTransactions(queryTransactions);
    } else {
      // restore transactions
      setTransactions(inMemoTransactions);
    }
    // normally there should be additional dep in dependency array - transactions,
    // but I am using a hack to return transactions after clearing-up filter - usually filtering and pagination happens on BE so this wouldnt happen
    // this is hacky coz the structure is for BE filtering. For FE we should just keep transactions and filter them on the display level, eg TransactionsList
    // eslint-disable-next-line
  }, [filter, inMemoTransactions]);

  return {
    isLoading,
    transactions,
    submitTransaction,
    removeTransaction,
    setFilter,
    setPagination,
  };
};
