import styles from './Transactions.module.css';
import { Box, Grid } from '@chakra-ui/react';
import { TransactionForm } from './TransactionForm';
import { Balance } from './Balance';
import { Filter } from './Filter';
import { TransactionsList } from './TransactionsList';
import { useTransactions } from './hooks';

export const TransactionsContainer = () => {
  // - react strictMode renders each components twice in strict mode on purpose, in prod version theres just one call
  const { transactions, submitTransaction, removeTransaction, setFilter } =
    useTransactions();

  return (
    <Grid className={styles.transactionsContainer}>
      <TransactionForm submitTransaction={submitTransaction} />
      <Box className={`${styles.balanceFilterContainer} ${styles.card}`}>
        <Balance />
        <Filter handleFiltering={setFilter} />
      </Box>
      <TransactionsList
        transactions={transactions}
        removeTransaction={removeTransaction}
      />
    </Grid>
  );
};
