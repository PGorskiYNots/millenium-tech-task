import styles from './Transactions.module.css';
import React, { useState } from 'react';
import { Box, Text, Flex, Button } from '@chakra-ui/react';
import { DeleteIcon } from '@chakra-ui/icons';

import type { Transaction } from './hooks/useTransactions';
interface TransacstionsListProps {
  transactions: Transaction[];
  removeTransaction: (id: string) => void;
}

export const TransactionsList = ({
  transactions = [],
  removeTransaction,
}: TransacstionsListProps) => {
  /* just a trick for task purposes, normal pagination would be a bit more complex */
  const sizePerPage = 3;
  const [count, setCount] = useState(sizePerPage);
  const handleSetOffset = () => {
    setCount(count + sizePerPage);
  };
  const transactionsToDisplay = transactions.slice(0, count);

  return (
    <Box className={`${styles.transactionsList} ${styles.card}`}>
      {transactionsToDisplay.map(transaction => (
        <Box key={transaction.id}>
          <Box textAlign="right" marginRight={8}>
            {' '}
            <DeleteIcon
              onClick={() => {
                if (typeof transaction.id !== 'undefined') {
                  removeTransaction(transaction.id);
                }
              }}
            />{' '}
          </Box>
          <Flex className={styles.transactionItem}>
            <Text> account: {transaction.account} </Text>
            <Text> address: {transaction.address} </Text>
            <Text> amount: {transaction.amount} </Text>
            <Text> beneficiary: {transaction.beneficiary} </Text>
            <Text> date: {transaction.date} </Text>
            <Text> description: {transaction.description} </Text>
          </Flex>
        </Box>
      ))}

      <Button m={4} colorScheme="teal" type="submit" onClick={handleSetOffset}>
        Load more
      </Button>
    </Box>
  );
};
