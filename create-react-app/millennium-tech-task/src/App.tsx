import React from 'react';
import { ChakraProvider, Box, theme } from '@chakra-ui/react';

import { Nav, Footer } from './modules/shared';
import { TransactionsContainer } from './modules/transactions';

const extendedTheme = {
  ...theme,
  breakPoints: {
    sm: '30em',
    md: '48em',
    lg: '62em',
    xl: '80em',
    '2xl': '96em',
  },
};

function App() {
  return (
    <ChakraProvider theme={extendedTheme}>
      <Nav />
      <Box
        textAlign="center"
        fontSize="xl"
        minHeight="calc(100vh - 88px - 88px)"
        backgroundColor="#f0f0f0"
      >
        <TransactionsContainer />
      </Box>
      <Footer />
    </ChakraProvider>
  );
}

export default App;
